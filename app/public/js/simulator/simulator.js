const portTICK_RATE_MS = 1;

function Simulator() {
	var initialised = false;
	var svgDoc = null;
	var motor_run_flag = false;
	var output1_timer = null;
	var _this = this;
	var simulator_running = false;
	var sound = new Sound();
	var mcp7940n = new Mcp7940n();
	// workers can not access objects => window, document, parent
	var ht16k33 = {
		worker: null,
		busy: false
	};

	this._constructor = function() {
		// on svg document loaded
		$('#svgDoc')[0].addEventListener('load', function() {
			svgDoc = $("#svgDoc")[0].contentDocument;

			// =================================================================
			// ht16k33 worker
			// =================================================================
			$("[id^=x]", svgDoc).css({
				fill: '#646464',
				stroke: '#404040'
			});
			if (typeof(Worker) !== 'undefined') {
				if (!ht16k33.worker) {
					ht16k33.worker = new Worker("js/simulator/workers/ht16k33.js");
					// ht16k33 worker message
					ht16k33.worker.onmessage = function(e) {
						var msg = e.data;
						switch (msg.type) {
							case 'busy_changed':
								ht16k33.busy = msg.busy;
								break;

							case 'render':
								$("#x" + msg.x + "y" + msg.y, svgDoc).css({
									fill: msg.f,
									stroke: msg.s
								});
								break;
						}
					};
				}
				// init ht16k33 worker
				ht16k33.worker.postMessage({
					type: 'init'
				});
			} else {
				//document.getElementById("result").innerHTML = "Sorry! No Web Worker support.";
			}

			// =================================================================
			// ldr
			// =================================================================
			ldr.get_cb = function() {
				//console.log($(".range#light")[0]); //.getCurrentStep() .getInfo()
				return parseFloat($(".range#light")[0].vGet());
			};

			// =================================================================
			// lm73
			// =================================================================
			lm73_1.get_cb = function() {
				return parseFloat($(".range#temperature")[0].vGet());
			};

			// =================================================================
			// ports
			// =================================================================
			// usbsw
			ports.usbsw_render_cb = function(val) {
				val == 0 ? $("#USB_LAMP_OFF", svgDoc).show() : $("#USB_LAMP_OFF", svgDoc).hide();
			};
			// output1
			$("#TIRE_PATTERN_2", svgDoc).hide();
			ports.output1_render_cb = function(val) {
				if (val == 0) {
					$("#TIRE_PATTERN_2", svgDoc).hide();
					$("#TIRE_PATTERN_1", svgDoc).show();
					if (output1_timer) {
						clearTimeout(output1_timer);
						output1_timer = null;
					}
				} else {
					if (output1_timer) {
						clearTimeout(output1_timer);
						output1_timer = null;
					}
					$("#TIRE_PATTERN_1", svgDoc).hide();
					$("#TIRE_PATTERN_2", svgDoc).show();
					motor_run_flag = true;
					output1_timer = setInterval(function() {
						motor_run_flag = !motor_run_flag;
						if (motor_run_flag) {
							$("#TIRE_PATTERN_1", svgDoc).hide();
							$("#TIRE_PATTERN_2", svgDoc).show();
						} else {
							$("#TIRE_PATTERN_2", svgDoc).hide();
							$("#TIRE_PATTERN_1", svgDoc).show();
						}
					}, 500);
				}
			};
			$("#LED_ON", svgDoc).hide();
			ports.output2_render_cb = function(val) {
				if (val == 0) {
					$("#LED_ON", svgDoc).hide();
					$("#LED_OFF", svgDoc).show();
				} else {
					$("#LED_OFF", svgDoc).hide();
					$("#LED_ON", svgDoc).show();
				}
			};
			// input1
			$("#SLIDE_DOOR_OPENED", svgDoc).hide();
			$("[id^=SLIDE_DOOR]", svgDoc).css('cursor', 'pointer');

			$("#SLIDE_DOOR_CLOSED", svgDoc).click(function(e) {
				$("#SLIDE_DOOR_CLOSED", svgDoc).hide();
				$("#SLIDE_DOOR_OPENED", svgDoc).show();
			});
			$("#SLIDE_DOOR_OPENED", svgDoc).click(function(e) {
				$("#SLIDE_DOOR_OPENED", svgDoc).hide();
				$("#SLIDE_DOOR_CLOSED", svgDoc).show();
			});
			ports.input1_read_cb = function() {
				if ($("#SLIDE_DOOR_OPENED", svgDoc).css('display') != 'none') {
					return 1;
				} else {
					return 0;
				}
			};
			// input2
			ports.input2_read_cb = function() {
				if ($("#INPUT_2 > text > tspan", svgDoc).text() == '0') {
					return 0;
				} else {
					return 1;
				}
			};
			// input3
			ports.input3_read_cb = function() {
				if ($("#INPUT_3 > text > tspan", svgDoc).text() == '0') {
					return 0;
				} else {
					return 1;
				}
			};
			// input4
			ports.input4_read_cb = function() {
				if ($("#INPUT_4 > text > tspan", svgDoc).text() == '0') {
					return 0;
				} else {
					return 1;
				}
			};

			// =================================================================
			// button12
			// =================================================================
			button12.sw1_get_cb = function() {
				if ($("#SWITCH_1 > text > tspan", svgDoc).text() == '0') {
					return 0;
				} else {
					return 1;
				}
			};
			button12.sw2_get_cb = function() {
				if ($("#SWITCH_2 > text > tspan", svgDoc).text() == '0') {
					return 0;
				} else {
					return 1;
				}
			};

			// =================================================================
			// sound
			// =================================================================
			sound.init();

			// =================================================================
			// sliders
			// =================================================================
			if (!initialised) {
				const temperature_values = [15, 60];
				const temperature_posfix = '℃';
				$(".range#temperature")
					.noUiSlider({
						start: 25,
						step: 0.25,
						range: {
							min: 10,
							max: 65
						}
					})
					.noUiSlider_pips({
						mode: 'values',
						density: 5,
						values: temperature_values,
						stepped: true,
						format: wNumb({
							decimals: 0,
							//prefix: '+',
							postfix: temperature_posfix
						})
					})
					.Link('lower').to('-inline-<div class="tooltip"></div>', function(value) {
						//$(this).html('<span>' + "+" + float2int(value) + "℃" + '</span>');
						$(this).html('<span>' + value + '&nbsp;' + temperature_posfix + '</span>');
					})
					.on('set', function(event, value) {
						if (value < temperature_values[0]) {
							$(this).val(temperature_values[0]);
						} else if (value > temperature_values[1]) {
							$(this).val(temperature_values[1]);
						}
					});
				$(".tail-bottom#temperature-tail-bottom").css('left', '60%');

				const light_values = [0, 100];
				const light_posfix = '%';
				$(".range#light")
					.noUiSlider({
						start: 50,
						step: 1,
						range: {
							min: 0,
							max: 100
						}
					})
					.noUiSlider_pips({
						mode: 'values',
						density: 5,
						values: light_values,
						stepped: true,
						format: wNumb({
							decimals: 0,
							//prefix: '+',
							postfix: light_posfix
						})
					})
					.Link('lower').to('-inline-<div class="tooltip"></div>', function(value) {
						//$(this).html('<span>' + "+" + float2int(value) + "℃" + '</span>');
						$(this).html('<span>' + parseInt(value) + '&nbsp;' + light_posfix + '</span>');
					})
					.on('set', function(event, value) {
						if (value < light_values[0]) {
							$(this).val(light_values[0]);
						} else if (value > light_values[1]) {
							$(this).val(light_values[1]);
						}
					});
				$(".tail-bottom#light-tail-bottom").css('left', '5%');
				// set tooltip visible
				$(".tooltip").css({
					'opacity': '1'
				});

				initialised = true;
			} else {
				$(".range#temperature")[0].vSet(25);
				$(".range#light")[0].vSet(50);
			}

			// =====================================================================
			// switches,inputs,outputs
			// =====================================================================
			/*$("#SWITCH_1", svgDoc).html('<title>Switch 1</title>');
			$("#SWITCH_2", svgDoc).html('<title>Switch 2</title>');
			$("#switch_reset", svgDoc).html('<title>Reset Switch</title>');*/
			// set cursor to pointer
			$("[id^=SWITCH_1],[id^=SWITCH_2],[id^=INPUT]", svgDoc).css('cursor', 'pointer');
			// disable text pointer-events
			$("[id^=SWITCH_2] > text,[id^=SWITCH_2] > text,[id^=INPUT] > text,[id^=OUTPUT] > text", svgDoc).css('pointer-events', 'none');
			// set switch hover in/out
			$("[id^=SWITCH_1],[id^=SWITCH_2],[id^=INPUT]", svgDoc).hover(function(e) {
				$(e.target).css({
					fill: '#f0f0f0'
				});
			}, function(e) {
				$(e.target).css({
					fill: '#151515'
				});
			});
			// set switch mouse down
			$("[id^=SWITCH_1],[id^=SWITCH_2]", svgDoc).mousedown(function(e) {
				var target = $(e.target).parent();
				var tspan = target.find('text > tspan');
				tspan.text('1');
			});
			// set switch mouse up
			$("[id^=SWITCH_1],[id^=SWITCH_2]", svgDoc).mouseup(function(e) {
				var target = $(e.target).parent();
				var tspan = target.find('text > tspan');
				tspan.text('0');
			});
			// set click
			$("[id^=INPUT]", svgDoc).click(function(e) {
				var target = $(e.target).parent();
				var tspan = target.find('text > tspan');
				if (tspan.text() == '0') {
					tspan.text('1');
				} else {
					tspan.text('0');
				}
			});

		});
	}

	this.run = function(code_str) {
		$('.modal-simulator').modal({
			show: true,
			keyboard: false,
			backdrop: 'static'
		});
		$('.modal-simulator button.close').click(function() {
			if (output1_timer) {
				clearTimeout(output1_timer);
				output1_timer = null;
			}
			$("#TIRE_PATTERN_2", svgDoc).hide();
			$("#TIRE_PATTERN_1", svgDoc).show();
			motor_run_flag = false;

			sound.off();
			simulator_running = false;
			$('.modal-simulator').modal('hide');
		});
		$('.modal-simulator').unbind('shown.bs.modal').on('shown.bs.modal', function() {
			var code_strlst = code_str.split('\n');
			var code = '';
			for (var i = 0; i < code_strlst.length; i++) {
				var line = code_strlst[i];
				if (line == '') {
					continue;
				}

				// remove c++ type casting
				line = line.replace(/\(uint8_t \*\)/g, '');
				line = line.replace(/\(double\)/g, '');
				line = line.replace(/\(char \*\)/g, '');
				line = line.replace(/\(int\)/g, '');
				// replace freertos tick
				line = line.replace(/portTICK_RATE_MS/g, portTICK_RATE_MS);

				// ht16k33.show javascript hex format
				if (line.indexOf('ht16k33.show') != -1) {
					var strlst = line.split('\\x');
					var inst = strlst[0];

					strlst[strlst.length - 1] = strlst[strlst.length - 1].replace(/\"\);/g, '');
					strlst.shift();
					var new_statement = '';
					for (var j in strlst) {
						if (strlst[j].length == 1) {
							new_statement += '\\x0' + strlst[j];
						} else {
							new_statement += '\\x' + strlst[j];
						}
					}

					line = inst + new_statement + '");';
				}

				code += line + '\n';
			}

			console.log(code);

			//==================================================================
			// JS-Interpreter
			//==================================================================
			var initFunc = function(interpreter, scope) {
				// =============================================================
				// freertos
				// =============================================================
				var vtaskdelay = function(val, callback) {
					setTimeout(function() {
						callback();
					}, val);
				};
				interpreter.setProperty(scope, 'vTaskDelay', interpreter.createAsyncFunction(vtaskdelay));

				// =============================================================
				// ht16k33
				// =============================================================
				HT16K33 = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'ht16k33', HT16K33);

				function ht16k33_idle_get() {
					return !ht16k33.busy;
				}
				var ht16k33_idle = function() {
					return interpreter.createPrimitive(ht16k33_idle_get());
				};
				interpreter.setProperty(HT16K33, 'idle', interpreter.createNativeFunction(ht16k33_idle));

				var ht16k33_wait_idle = function(callback) {
					function wait_idle() {
						if (ht16k33.busy) {
							setTimeout(wait_idle, 50);
						} else {
							callback();
						}
					}
					wait_idle();
				};
				interpreter.setProperty(HT16K33, 'wait_idle', interpreter.createAsyncFunction(ht16k33_wait_idle));

				var ht16k33_show = function(buf) {
					return interpreter.createPrimitive(
						ht16k33.worker.postMessage({
							type: 'show',
							buf: buf
						})
					);
				};
				interpreter.setProperty(HT16K33, 'show', interpreter.createNativeFunction(ht16k33_show));

				var ht16k33_scroll = function(buf, scroll_flag) {
					return interpreter.createPrimitive(
						ht16k33.worker.postMessage({
							type: 'scroll',
							buf: buf,
							scroll_flag: scroll_flag
						})
					);
				};
				interpreter.setProperty(HT16K33, 'scroll', interpreter.createNativeFunction(ht16k33_scroll));

				// =============================================================
				// ldr
				// =============================================================
				LDR = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'ldr', LDR);

				var ldr_get = function(buf) {
					return interpreter.createPrimitive(ldr.get());
				};
				interpreter.setProperty(LDR, 'get', interpreter.createNativeFunction(ldr_get));

				// =============================================================
				// lm73
				// =============================================================
				LM73_1 = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'lm73_1', LM73_1);

				var lm73_1_get = function(buf) {
					return interpreter.createPrimitive(lm73_1.get());
				};
				interpreter.setProperty(LM73_1, 'get', interpreter.createNativeFunction(lm73_1_get));

				LM73_0 = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'lm73_0', LM73_0);

				var lm73_0_get = function(buf) {
					return interpreter.createPrimitive(lm73_0.get());
				};
				interpreter.setProperty(LM73_0, 'get', interpreter.createNativeFunction(lm73_0_get));

				var lm73_0_error = function(buf) {
					return interpreter.createPrimitive(lm73_0.error());
				};
				interpreter.setProperty(LM73_0, 'error', interpreter.createNativeFunction(lm73_0_error));

				// =============================================================
				// ports
				// =============================================================
				PORTS = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'ports', PORTS);

				var ports_usbsw_write = function(val) {
					return interpreter.createPrimitive(ports.usbsw_write(val));
				};
				interpreter.setProperty(PORTS, 'usbsw_write', interpreter.createNativeFunction(ports_usbsw_write));

				var ports_usbsw_toggle = function() {
					return interpreter.createPrimitive(ports.usbsw_toggle());
				};
				interpreter.setProperty(PORTS, 'usbsw_toggle', interpreter.createNativeFunction(ports_usbsw_toggle));

				var ports_usbsw_read = function() {
					return interpreter.createPrimitive(ports.usbsw_read());
				};
				interpreter.setProperty(PORTS, 'usbsw_read', interpreter.createNativeFunction(ports_usbsw_read));

				var ports_output1_write = function(val) {
					return interpreter.createPrimitive(ports.output1_write(val));
				};
				interpreter.setProperty(PORTS, 'output1_write', interpreter.createNativeFunction(ports_output1_write));

				var ports_output1_toggle = function() {
					return interpreter.createPrimitive(ports.output1_toggle());
				};
				interpreter.setProperty(PORTS, 'output1_toggle', interpreter.createNativeFunction(ports_output1_toggle));

				var ports_output1_read = function() {
					return interpreter.createPrimitive(ports.output1_read());
				};
				interpreter.setProperty(PORTS, 'output1_read', interpreter.createNativeFunction(ports_output1_read));

				var ports_output2_write = function(val) {
					return interpreter.createPrimitive(ports.output2_write(val));
				};
				interpreter.setProperty(PORTS, 'output2_write', interpreter.createNativeFunction(ports_output2_write));

				var ports_output2_toggle = function() {
					return interpreter.createPrimitive(ports.output2_toggle());
				};
				interpreter.setProperty(PORTS, 'output2_toggle', interpreter.createNativeFunction(ports_output2_toggle));

				var ports_output2_read = function() {
					return interpreter.createPrimitive(ports.output2_read());
				};
				interpreter.setProperty(PORTS, 'output2_read', interpreter.createNativeFunction(ports_output2_read));

				var ports_input1_read = function() {
					return interpreter.createPrimitive(ports.input1_read());
				};
				interpreter.setProperty(PORTS, 'input1_read', interpreter.createNativeFunction(ports_input1_read));

				var ports_input2_read = function() {
					return interpreter.createPrimitive(ports.input2_read());
				};
				interpreter.setProperty(PORTS, 'input2_read', interpreter.createNativeFunction(ports_input2_read));

				var ports_input3_read = function() {
					return interpreter.createPrimitive(ports.input3_read());
				};
				interpreter.setProperty(PORTS, 'input3_read', interpreter.createNativeFunction(ports_input3_read));

				var ports_input4_read = function() {
					return interpreter.createPrimitive(ports.input4_read());
				};
				interpreter.setProperty(PORTS, 'input4_read', interpreter.createNativeFunction(ports_input4_read));

				// =============================================================
				// button12
				// =============================================================
				BUTTON12 = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'button12', BUTTON12);

				var button12_sw1_get = function() {
					return interpreter.createPrimitive(button12.sw1_get());
				};
				interpreter.setProperty(BUTTON12, 'sw1_get', interpreter.createNativeFunction(button12_sw1_get));

				var button12_sw2_get = function() {
					return interpreter.createPrimitive(button12.sw2_get());
				};
				interpreter.setProperty(BUTTON12, 'sw2_get', interpreter.createNativeFunction(button12_sw2_get));

				var button12_sw2_get = function() {
					return interpreter.createPrimitive(button12.sw2_get());
				};
				interpreter.setProperty(BUTTON12, 'sw2_get', interpreter.createNativeFunction(button12_sw2_get));

				var button12_wait_sw1_pressed = function(callback) {
					function wait_sw1_pressed() {
						if (button12.sw1_get() == 0) {
							setTimeout(wait_sw1_pressed, 50);
						} else {
							callback();
						}
					}
					wait_sw1_pressed();
				};
				interpreter.setProperty(BUTTON12, 'wait_sw1_pressed', interpreter.createAsyncFunction(button12_wait_sw1_pressed));

				var button12_wait_sw1_released = function(callback) {
					function wait_sw1_released() {
						if (button12.sw1_get() == 0) {
							setTimeout(wait_sw1_released, 50);
						} else {
							callback();
						}
					}
					wait_sw1_released();
				};
				interpreter.setProperty(BUTTON12, 'wait_sw1_released', interpreter.createAsyncFunction(button12_wait_sw1_released));

				var button12_wait_sw2_pressed = function(callback) {
					function wait_sw2_pressed() {
						if (button12.sw2_get() == 0) {
							setTimeout(wait_sw2_pressed, 50);
						} else {
							callback();
						}
					}
					wait_sw2_pressed();
				};
				interpreter.setProperty(BUTTON12, 'wait_sw2_pressed', interpreter.createAsyncFunction(button12_wait_sw2_pressed));

				var button12_wait_sw2_released = function(callback) {
					function wait_sw2_released() {
						if (button12.sw2_get() == 0) {
							setTimeout(wait_sw2_released, 50);
						} else {
							callback();
						}
					}
					wait_sw2_released();
				};
				interpreter.setProperty(BUTTON12, 'wait_sw2_released', interpreter.createAsyncFunction(button12_wait_sw2_released));

				var button12_is_sw1_pressed = function() {
					return interpreter.createPrimitive(button12.is_sw1_pressed());
				};
				interpreter.setProperty(BUTTON12, 'is_sw1_pressed', interpreter.createNativeFunction(button12_is_sw1_pressed));

				var button12_is_sw1_released = function() {
					return interpreter.createPrimitive(button12.is_sw1_released());
				};
				interpreter.setProperty(BUTTON12, 'is_sw1_released', interpreter.createNativeFunction(button12_is_sw1_released));

				var button12_is_sw2_pressed = function() {
					return interpreter.createPrimitive(button12.is_sw2_pressed());
				};
				interpreter.setProperty(BUTTON12, 'is_sw2_pressed', interpreter.createNativeFunction(button12_is_sw2_pressed));

				var button12_is_sw2_released = function() {
					return interpreter.createPrimitive(button12.is_sw2_released());
				};
				interpreter.setProperty(BUTTON12, 'is_sw2_released', interpreter.createNativeFunction(button12_is_sw2_released));

				var button12_key_pressed = function() {
					return interpreter.createPrimitive(button12.key_pressed());
				};
				interpreter.setProperty(BUTTON12, 'key_pressed', interpreter.createNativeFunction(button12_key_pressed));

				var button12_key_released = function() {
					return interpreter.createPrimitive(button12.key_released());
				};
				interpreter.setProperty(BUTTON12, 'key_released', interpreter.createNativeFunction(button12_key_released));

				// =============================================================
				// kbiot
				// =============================================================
				var kbiot_get_B1state = function() {
					return interpreter.createPrimitive(get_B1state());
				};
				interpreter.setProperty(scope, 'get_B1state', interpreter.createNativeFunction(kbiot_get_B1state));

				var kbiot_get_B2state = function() {
					return interpreter.createPrimitive(get_B2state());
				};
				interpreter.setProperty(scope, 'get_B2state', interpreter.createNativeFunction(kbiot_get_B2state));

				var kbiot_get_B1stateClicked = function() {
					return interpreter.createPrimitive(get_B1stateClicked());
				};
				interpreter.setProperty(scope, 'get_B1stateClicked', interpreter.createNativeFunction(kbiot_get_B1stateClicked));

				var kbiot_get_B2stateClicked = function() {
					return interpreter.createPrimitive(get_B2stateClicked());
				};
				interpreter.setProperty(scope, 'get_B2stateClicked', interpreter.createNativeFunction(kbiot_get_B2stateClicked));

				var kbiot_set_B1release = function() {
					return interpreter.createPrimitive(set_B1release());
				};
				interpreter.setProperty(scope, 'set_B1release', interpreter.createNativeFunction(kbiot_set_B1release));

				var kbiot_set_B2release = function() {
					return interpreter.createPrimitive(set_B2release());
				};
				interpreter.setProperty(scope, 'set_B2release', interpreter.createNativeFunction(kbiot_set_B2release));

				// =============================================================
				// sound
				// =============================================================
				SOUND = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'sound', SOUND);

				var sound_note = function(note) {
					return interpreter.createPrimitive(sound.note(note));
				};
				interpreter.setProperty(SOUND, 'note', interpreter.createNativeFunction(sound_note));

				var sound_rest = function(duration, callback) {
					var bpm = 120;
					var quarter_delay;
					var delay = 0;
					/*
					[120 bpm]
					whole = 2000 ms
					haft = 1000 ms
					quarter delay = 60*1000/120 = 500 ms
					eighth = 250 ms
					sixteenth = 125
					*/
					quarter_delay = (60 * 1000) / bpm;
					switch (duration) {
						case 0:
							delay = 4 * quarter_delay;
							break;

						case 1:
							delay = 2 * quarter_delay;
							break;

						case 2:
							delay = quarter_delay;
							break;

						case 3:
							delay = quarter_delay / 2;
							break;

						case 4:
							delay = quarter_delay / 4;
							break;

						default:
							delay = quarter_delay / 4;
							break;
					}

					if (delay > 0) {
						sound.oscillator.stop(sound.currentTime + (delay / 1000));
						sound.oscillator.onended = function(e) {
							callback();
						}
					} else {
						callback();
					}
				};
				interpreter.setProperty(SOUND, 'rest', interpreter.createAsyncFunction(sound_rest));

				var sound_off = function() {
					return interpreter.createPrimitive(sound.off());
				};
				interpreter.setProperty(SOUND, 'off', interpreter.createNativeFunction(sound_off));

				var sound_get_volume = function() {
					return interpreter.createPrimitive(sound.get_volume());
				};
				interpreter.setProperty(SOUND, 'get_volume', interpreter.createNativeFunction(sound_get_volume));

				var sound_set_volume = function(val) {
					return interpreter.createPrimitive(sound.set_volume(val));
				};
				interpreter.setProperty(SOUND, 'set_volume', interpreter.createNativeFunction(sound_set_volume));

				// =============================================================
				// mcp7940n
				// =============================================================
				MCP7940N = interpreter.createObject(interpreter.OBJECT);
				interpreter.setProperty(scope, 'mcp7940n', MCP7940N);

				var mcp7940n_get_datetime = function() {
					return interpreter.createPrimitive(mcp7940n.get_datetime());
				};
				interpreter.setProperty(MCP7940N, 'get_datetime', interpreter.createNativeFunction(mcp7940n_get_datetime));

				var mcp7940n_get_datetime_with_second = function() {
					return interpreter.createPrimitive(mcp7940n.get_datetime_with_second());
				};
				interpreter.setProperty(MCP7940N, 'get_datetime_with_second', interpreter.createNativeFunction(mcp7940n_get_datetime_with_second));

				var mcp7940n_get_date = function() {
					return interpreter.createPrimitive(mcp7940n.get_date());
				};
				interpreter.setProperty(MCP7940N, 'get_date', interpreter.createNativeFunction(mcp7940n_get_date));

				var mcp7940n_get_time = function() {
					return interpreter.createPrimitive(mcp7940n.get_time());
				};
				interpreter.setProperty(MCP7940N, 'get_time', interpreter.createNativeFunction(mcp7940n_get_time));

				var mcp7940n_get_time_with_second = function() {
					return interpreter.createPrimitive(mcp7940n.get_time_with_second());
				};
				interpreter.setProperty(MCP7940N, 'get_time_with_second', interpreter.createNativeFunction(mcp7940n_get_time_with_second));

				var mcp7940n_get = function() {
					return interpreter.createPrimitive(mcp7940n.get());
				};
				interpreter.setProperty(MCP7940N, 'get', interpreter.createNativeFunction(mcp7940n_get));
			}

			var interpreter = new Interpreter(code, initFunc);
			simulator_running = true;
			setTimeout(function () {
				function main_stepInterpreter() {
					try {
						var ok = interpreter.step();
						if (simulator_running) {
							setTimeout(main_stepInterpreter, 0);
						}
					} finally {
						if (!ok) {

						}
					}
				}
				main_stepInterpreter();
			}, 1000);

		});
	}

	// initialize
	this._constructor();
}
